from fileinput import filename
from cv import IAnalyzer
import json
import requests


class Metadefender(IAnalyzer):
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
            
    def analyze(self):
               
        header={'apikey':'09be200cc2c0415e6523c2a4b2b37258'}
        url='https://api.metadefender.com/v4/hash/'+self.filename+'/peinfo'
        r = requests.get(url,headers=header)
        res = json.loads(r.text)
       
        result_json={}
        intermediar_json={}
        if 'headers' in res:
            header=res['headers']
        
            for key in header:
                intermediar_json[key]=header[key]
        
        
            result_json['Header']=intermediar_json
            
        intermediar_json={}
        
        if 'imported_dlls' in res:
            result_json['Imported Dlls']=res['imported_dlls']  
            
        # if 'Exported Functions' in res:    
        #     result_json['Exported Functions']=res['exported_functions']
         
        return result_json
        
              
    