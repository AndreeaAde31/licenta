#punem toate info din pefile
import os
import sys
import pickle
import argparse
import numpy
import pefile
import array
import math
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier

from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score
from sklearn.model_selection import train_test_split

from sklearn.naive_bayes import GaussianNB
from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
import xgboost as xgb



model = {   "DecisionTree":tree.DecisionTreeClassifier(max_depth=10),
            "RandomForest":RandomForestClassifier(n_estimators=64),
            "Adaboost": AdaBoostClassifier(n_estimators=50),
            "ExtremeGradientBoosting":xgb.XGBClassifier(random_state=123),
            "LogisticRegression":LogisticRegression(max_iter=10000)   
        }  


def get_entropy(data):
    if len(data) == 0:
        return 0.0
    occurences = array.array('L', [0]*256)
    for x in data:
        occurences[x if isinstance(x, int) else ord(x)] += 1
    entropy = 0
    for x in occurences:
        if x:
            p_x = float(x) / len(data)
            entropy -= p_x*math.log(p_x, 2)
    return entropy

def get_resources(pe):
    """Extract resources :
    [entropy, size]"""
    resources = []
    if hasattr(pe, 'DIRECTORY_ENTRY_RESOURCE'):
        try:
            for resource_type in pe.DIRECTORY_ENTRY_RESOURCE.entries:
                if hasattr(resource_type, 'directory'):
                    for resource_id in resource_type.directory.entries:
                        if hasattr(resource_id, 'directory'):
                            for resource_lang in resource_id.directory.entries:
                                data = pe.get_data(resource_lang.data.struct.OffsetToData, resource_lang.data.struct.Size)
                                size = resource_lang.data.struct.Size
                                entropy = get_entropy(data)

                                resources.append([entropy, size])
        except Exception as e:
            return resources
    return resources

def get_version_info(pe):
    """Return version infos"""
    res = {}
    for fileinfo in pe.FileInfo:
        if fileinfo.Key == 'StringFileInfo':
            for st in fileinfo.StringTable:
                for entry in st.entries.items():
                    res[entry[0]] = entry[1]
        if fileinfo.Key == 'VarFileInfo':
            for var in fileinfo.Var:
                res[var.entry.items()[0][0]] = var.entry.items()[0][1]
    if hasattr(pe, 'VS_FIXEDFILEINFO'):
        res['flags'] = pe.VS_FIXEDFILEINFO.FileFlags
        res['os'] = pe.VS_FIXEDFILEINFO.FileOS
        res['type'] = pe.VS_FIXEDFILEINFO.FileType
        res['file_version'] = pe.VS_FIXEDFILEINFO.FileVersionLS
        res['product_version'] = pe.VS_FIXEDFILEINFO.ProductVersionLS
        res['signature'] = pe.VS_FIXEDFILEINFO.Signature
        res['struct_version'] = pe.VS_FIXEDFILEINFO.StrucVersion
    return res

def get_string_features(path):
     
    pe=pefile.PE(path)
    features = {}
    features['Machine'] = pe.FILE_HEADER.Machine
    features['Characteristics'] = pe.FILE_HEADER.Characteristics    
    features['SizeOfOptionalHeader'] = pe.FILE_HEADER.SizeOfOptionalHeader    
    
    features['ImageBase'] = pe.OPTIONAL_HEADER.ImageBase
    features['DllCharacteristics'] = pe.OPTIONAL_HEADER.DllCharacteristics    
    features['Subsystem'] = pe.OPTIONAL_HEADER.Subsystem    
    
    features['MajorLinkerVersion'] = pe.OPTIONAL_HEADER.MajorLinkerVersion
    features['MinorLinkerVersion'] = pe.OPTIONAL_HEADER.MinorLinkerVersion
    features['SizeOfCode'] = pe.OPTIONAL_HEADER.SizeOfCode
    features['SizeOfInitializedData'] = pe.OPTIONAL_HEADER.SizeOfInitializedData
    features['SizeOfUninitializedData'] = pe.OPTIONAL_HEADER.SizeOfUninitializedData
    features['AddressOfEntryPoint'] = pe.OPTIONAL_HEADER.AddressOfEntryPoint
    features['BaseOfCode'] = pe.OPTIONAL_HEADER.BaseOfCode
    
    try:
        features['BaseOfData'] = pe.OPTIONAL_HEADER.BaseOfData
    except AttributeError:
        features['BaseOfData'] = 0
        
    features['SectionAlignment'] = pe.OPTIONAL_HEADER.SectionAlignment
    features['FileAlignment'] = pe.OPTIONAL_HEADER.FileAlignment
    features['MajorOperatingSystemVersion'] = pe.OPTIONAL_HEADER.MajorOperatingSystemVersion
    features['MinorOperatingSystemVersion'] = pe.OPTIONAL_HEADER.MinorOperatingSystemVersion
    features['MajorImageVersion'] = pe.OPTIONAL_HEADER.MajorImageVersion
    features['MinorImageVersion'] = pe.OPTIONAL_HEADER.MinorImageVersion
    features['MajorSubsystemVersion'] = pe.OPTIONAL_HEADER.MajorSubsystemVersion
    features['MinorSubsystemVersion'] = pe.OPTIONAL_HEADER.MinorSubsystemVersion
    features['SizeOfImage'] = pe.OPTIONAL_HEADER.SizeOfImage
    features['SizeOfHeaders'] = pe.OPTIONAL_HEADER.SizeOfHeaders
    features['CheckSum'] = pe.OPTIONAL_HEADER.CheckSum    
    
    features['SizeOfStackReserve'] = pe.OPTIONAL_HEADER.SizeOfStackReserve
    features['SizeOfStackCommit'] = pe.OPTIONAL_HEADER.SizeOfStackCommit
    features['SizeOfHeapReserve'] = pe.OPTIONAL_HEADER.SizeOfHeapReserve
    features['SizeOfHeapCommit'] = pe.OPTIONAL_HEADER.SizeOfHeapCommit
    features['LoaderFlags'] = pe.OPTIONAL_HEADER.LoaderFlags
    features['NumberOfRvaAndSizes'] = pe.OPTIONAL_HEADER.NumberOfRvaAndSizes
    
    
    #Imports
    try:
        features['ImportsNbDLL'] = len(pe.DIRECTORY_ENTRY_IMPORT)
        imports = sum([x.imports for x in pe.DIRECTORY_ENTRY_IMPORT], [])
        features['ImportsNb'] = len(imports)
        features['ImportsNbOrdinal'] = len(list(filter(lambda x:x.name is None, imports)))
        
    except AttributeError:
        features['ImportsNbDLL'] = 0
        features['ImportsNb'] = 0
        features['ImportsNbOrdinal'] = 0
    
    
    #Exports
    try:
        features['ExportNb'] = len(pe.DIRECTORY_ENTRY_EXPORT.symbols)
    except AttributeError:
        # No export
        features['ExportNb'] = 0
    
    
    #Resources
    resources= get_resources(pe)
    features['ResourcesNb'] = len(resources)
    if len(resources)> 0:
        entropy=list(map(lambda x:x[0], resources))
        features['ResourcesMeanEntropy'] = sum(entropy)/float(len(entropy))
        features['ResourcesMinEntropy'] = min(entropy)
        features['ResourcesMaxEntropy'] = max(entropy) 
        sizes = list(map(lambda x:x[1], resources))
        features['ResourcesMeanSize'] = sum(sizes)/float(len(sizes))
        features['ResourcesMinSize'] = min(sizes)
        features['ResourcesMaxSize'] = max(sizes)
        
    else:
        
        features['ResourcesMeanEntropy'] = 0
        features['ResourcesMinEntropy'] = 0
        features['ResourcesMaxEntropy'] = 0
        features['ResourcesMeanSize'] = 0
        features['ResourcesMinSize'] = 0
        features['ResourcesMaxSize'] = 0
    
    
    # Sections
    features['SectionsNb']=len(pe.sections)
    entropy = list(map(lambda x:x.get_entropy(), pe.sections))
    if len(entropy)> 0:
        features['SectionsMeanEntropy'] = sum(entropy)/float(len(entropy))
        features['SectionsMinEntropy'] = min(entropy)
        features['SectionsMaxEntropy'] = max(entropy)
        
    else:
        
        features['SectionsMeanEntropy'] = 0
        features['SectionsMinEntropy'] = 0
        features['SectionsMaxEntropy'] = 0
    
    raw_sizes = list(map(lambda x:x.SizeOfRawData, pe.sections))
    if len(raw_sizes)>0:
        features['SectionsMeanRawsize'] = sum(raw_sizes)/float(len(raw_sizes))
        features['SectionsMinRawsize'] = min(raw_sizes)
        features['SectionsMaxRawsize'] = max(raw_sizes)   
    
    else:
        features['SectionsMeanRawsize'] = 0
        features['SectionsMinRawsize'] = 0
        features['SectionsMaxRawsize'] = 0 
     
     
    virtual_sizes = list(map(lambda x:x.Misc_VirtualSize, pe.sections)) 
    if len(virtual_sizes)>0:
        features['SectionsMeanVirtualsize'] = sum(virtual_sizes)/float(len(virtual_sizes))
        features['SectionsMinVirtualsize'] = min(virtual_sizes)
        features['SectionMaxVirtualsize'] = max(virtual_sizes)
    
    else: 
        features['SectionsMeanVirtualsize'] = 0
        features['SectionsMinVirtualsize'] = 0
        features['SectionMaxVirtualsize'] = 0
     
     
     
    # Load configuration size
    try:
        features['LoadConfigurationSize'] = pe.DIRECTORY_ENTRY_LOAD_CONFIG.struct.Size
    except AttributeError:
        features['LoadConfigurationSize'] = 0 
        
        
    # Version configuration size
    try:
        version_infos = get_version_info(pe)
        features['VersionInformationSize'] = len(version_infos.keys())
    except AttributeError:
        features['VersionInformationSize'] = 0
    

    #TRANSFORMAM features in numpy.array
    mylist=[]
    for key in features:
        mylist.append(features[key])
    
    arr = numpy.array(mylist)
        
    return arr

def scan_file(path):
    # scan a file to determine if it is malicious or benign
    if not os.path.exists(".\\MLCode\\54.pkl"):
        print("It appears you haven't trained a detector yet!  Do this before scanning files.")
        sys.exit(1)
         
    with open(".\\MLCode\\54.pkl", 'rb') as saved_detector:
        classifier = pickle.load(saved_detector)
    
    try:
        features = get_string_features(path)
        result_proba = classifier.predict_proba([features])[:,1]   
        print(str(result_proba.tolist()[0]*100))
           
    except pefile.PEFormatError as err:
        print("DOS Header magic not found")    
        
    

def train_detector(benign_path,malicious_path):
    # train the detector on the specified training data
    def get_training_paths(directory):
        targets = []
        for path in os.listdir(directory):
            targets.append(os.path.join(directory,path))
        return targets
    malicious_paths = get_training_paths(malicious_path)
    benign_paths = get_training_paths(benign_path)
    X=[]
    i=0
    j=0
    for path in malicious_paths: 
        try:
            X.append(get_string_features(path))
            i+=1
        except pefile.PEFormatError as err:
            continue
    
    for path in benign_paths: 
        try:
            X.append(get_string_features(path))
            j+=1
        except pefile.PEFormatError as err:
            continue
    
    y = [1 for k in range(i)] + [0 for i in range(j)]
    
    X_train, X_test, y_train, y_test = train_test_split(X, y ,test_size=0.2)
    results = {}
    for algo in model:
        clf = model[algo]
        clf.fit(X_train,y_train)
        score = clf.score(X_test,y_test)
        results[algo] = score
    
    winner = max(results, key=results.get)
    pickle.dump((model[winner]),open("54.pkl","wb"))

def report(y_test, y_pred):
    tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
    tpr = tp / (tp + fn)
    pv = tp / (tp + fp)
    f_score = 2 * tpr * pv / (tpr + pv)
    return float("{:.2f}".format(f_score))


def cv_evaluate(X,y):
    # use cross-validation to evaluate our model
    from sklearn import metrics
    from matplotlib import pyplot
    from sklearn.model_selection import KFold
    X, y = numpy.array(X), numpy.array(y)
    kf = KFold(5,shuffle=True)
  
    results = {}
    for algo in model:
        clf = model[algo]
        fold_counter = 0
        for train, test in kf.split(X):
            training_X, training_y = X[train], y[train]
            test_X, test_y = X[test], y[test]
            
            clf.fit(training_X,training_y)
            scores = clf.predict_proba(test_X)[:,-1]
            fpr, tpr, thresholds = metrics.roc_curve(test_y, scores)
            pyplot.semilogx(fpr,tpr,label="Fold number {0}".format(fold_counter))    
            fold_counter += 1
        
        # score=clf.score(test_X, test_y)   
        # results[algo] = score
        # print("%f %%" % (score*100))
        predictions=clf.predict(test_X)
        # print(algo + " F1-score: %.6f" % f1_score(test_y, predictions, average='weighted') )
        f_score=report(test_y, predictions)   
        print(algo, f_score)
    

def get_training_data(benign_path,malicious_path):
    def get_training_paths(directory):
        targets = []
        for path in os.listdir(directory):
            targets.append(os.path.join(directory,path))
        return targets
    malicious_paths = get_training_paths(malicious_path)
    benign_paths = get_training_paths(benign_path)
    
    X=[]
    i=0
    j=0
    for path in malicious_paths: 
        try:
            X.append(get_string_features(path))
            i+=1
        except pefile.PEFormatError as err:
            continue
    
    for path in benign_paths: 
        try:
            X.append(get_string_features(path))
            j+=1
        except pefile.PEFormatError as err:
            continue
    
    y = [1 for k in range(i)] + [0 for i in range(j)]
       
    return X, y

parser = argparse.ArgumentParser("get windows object vectors for files")
parser.add_argument("--malware_paths",default=None,help="Path to malware training files")
parser.add_argument("--benignware_paths",default=None,help="Path to benignware training files")
parser.add_argument("--scan_file_path",default=None,help="File to scan")
parser.add_argument("--evaluate",default=False,action="store_true",help="Perform cross-validation")

args = parser.parse_args()

if args.malware_paths and args.benignware_paths and not args.evaluate:
    train_detector(args.benignware_paths,args.malware_paths)
elif args.scan_file_path:
    scan_file(args.scan_file_path)
elif args.malware_paths and args.benignware_paths and args.evaluate:
    X, y = get_training_data(args.benignware_paths,args.malware_paths)
    cv_evaluate(X,y)
else:
    print("No specified path to malware/benign files or no file to scan")
