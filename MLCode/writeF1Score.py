
import sys
import os 
from sklearn.metrics import f1_score
sys.path.insert(1, '../')
from cv import IAnalyzer
from createDB import createPerformance

class WriteF1Score(IAnalyzer):
    
    def __init__(self, benignware_paths, malware_paths):
        super().__init__()
        self.benignware_paths=benignware_paths
        self.malware_paths=malware_paths
        
            
    def analyze(self):
        print("sunt in strings")
        cmd="python strings.py --benignware_paths '" + self.benignware_paths + "' --malware_paths '"+self.malware_paths+ "' --evaluate" 
        output=self.run(cmd)
        lista=list(output.split("\n"))
        
        for i in lista[:-1]:
            row=list(i.split(" "))
            detectorType=row[0]
            score=row[-1].replace("\r", "")         
            createPerformance(detectorType, score, "strings")
        
        #-------------------------------------------------------------------------
        print("sunt in 54")
        cmd="python features-54.py --benignware_paths '" + self.benignware_paths + "' --malware_paths '"+self.malware_paths+ "' --evaluate" 
        output=self.run(cmd)
        lista=list(output.split("\n"))
        
        for i in lista[:-1]:
            row=list(i.split(" "))
            detectorType=row[0]
            score=row[-1].replace("\r", "")        
            createPerformance(detectorType, score, "54header")
        
        # #-------------------------------------------------------------------------
        print("sunt in PCA")
        cmd="python features-PCA.py --benignware_paths '" + self.benignware_paths + "' --malware_paths '"+self.malware_paths+ "' --evaluate" 
        output=self.run(cmd)
        lista=list(output.split("\n"))
        
        for i in lista[:-1]:
            row=list(i.split(" "))
            detectorType=row[0]
            score=row[-1].replace("\r", "")          
            createPerformance(detectorType, score, "PCAheader")
        
        
obj=WriteF1Score("C:\\Users\\Ade\\Downloads\\myfiles\\benignware", "C:\\Users\\Ade\\Downloads\\myfiles\\malware")
obj.analyze()    
  