
import sys
sys.path.insert(1, '../')

from cv import IAnalyzer

class MachineLearning(IAnalyzer):
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
        
            
    def analyze(self):
        
        final={}
        cmd="python .\\MLCode\\strings.py --scan_file_path '" + self.filename + "' "
        output=self.run(cmd)
        lista=list(output.split("\n"))
        no_str=int(lista[0].replace("\r", ""))
        probability1=float(lista[1].replace("\r", ""))
        print(output)

        #-------------------------------------------------------------------------
        cmd="python .\\MLCode\\features-54.py --scan_file_path '" + self.filename + "' "
        output=""
        output=self.run(cmd)
        print(output)
        
        lista=list(output.split("\n"))
        probability2=float(lista[0].replace("\r", ""))
        #-------------------------------------------------------------------------
        cmd="python .\\MLCode\\features-PCA.py --scan_file_path '" + self.filename + "' "
        output=""
        output=self.run(cmd)
        print(output)
        lista=list(output.split("\n"))
        probability3=float(lista[0].replace("\r", ""))
        
        final={}
        final['Extracted strings']=no_str
        final['Probability']=(probability1+probability2+probability3)/3
        
        return final
                       
# tool=MachineLearning("C:\\Users\\Ade\\Desktop\\Licenta\\Proiect\\uploads\\b284b6cc53f3fd14bbe580e7f2313d2647c53158069c7249e86edb4da749e408.exe")
# print(tool.analyze())    
  