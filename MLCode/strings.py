import os
import sys
import pickle
import argparse
import re
import numpy
from sklearn.feature_extraction import FeatureHasher
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score
from sklearn.model_selection import train_test_split

from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
import xgboost as xgb

model = {   "DecisionTree":tree.DecisionTreeClassifier(max_depth=10),
            "RandomForest":RandomForestClassifier(n_estimators=64),
            "Adaboost": AdaBoostClassifier(n_estimators=50),
            "ExtremeGradientBoosting":xgb.XGBClassifier(random_state=123),
            "LogisticRegression":LogisticRegression()   
        }  


def get_string_features(path,hasher,flag=0):
    # extract strings from binary file with regex
    chars = r" -~"
    min_length = 5
    string_regexp = '[%s]{%d,}' % (chars, min_length)
    
    file_object = open(path, errors="ignore")
    data = file_object.read()
    pattern = re.compile(string_regexp)
    strings = pattern.findall(data)

    # dictionary form
    string_features = {}
    for string in strings:
        string_features[string] = 1
   
    
    # hashing trick
    hashed_features = hasher.transform([string_features])
    hashed_features = hashed_features.todense()
    hashed_features = numpy.asarray(hashed_features)
    hashed_features = hashed_features[0]
    
    # return hashed string features
    if flag==1 :
        return len(string_features), hashed_features
    else:    
        return hashed_features

def scan_file(path):
    
    if not os.path.exists(".\MLCode\strings.pkl"):
        print("No trained a detector file! Train before scanning files.")
        sys.exit(1)
         
    with open(".\MLCode\strings.pkl", 'rb') as saved_detector:
        classifier, hasher = pickle.load(saved_detector)
    no_strings, features = get_string_features(path,hasher,1)
    result_proba = classifier.predict_proba([features])[:,1]
    
    print(no_strings)
    print(str(result_proba.tolist()[0]*100))
    

def train_detector(benign_path,malicious_path,hasher):
    
    def get_training_paths(directory):
        targets = []
        for path in os.listdir(directory):
            targets.append(os.path.join(directory,path))
        return targets
    malicious_paths = get_training_paths(malicious_path)
    benign_paths = get_training_paths(benign_path)
    X = [get_string_features(path,hasher) for path in malicious_paths + benign_paths]

    y = [1 for i in range(len(malicious_paths))] + [0 for i in range(len(benign_paths))]
    
    
    X_train, X_test, y_train, y_test = train_test_split(X, y ,test_size=0.2)
    results = {}
    for algo in model:
        clf = model[algo]
        clf.fit(X,y)
        score = clf.score(X_test,y_test)
        print ("%s : %s " %(algo, score))
        results[algo] = score
    
    winner = max(results, key=results.get)
    pickle.dump((model[winner],hasher),open("strings.pkl","wb")) 

def report(y_test, y_pred):
    tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
    tpr = tp / (tp + fn)
    pv = tp / (tp + fp)
    f_score = 2 * tpr * pv / (tpr + pv)
    return float("{:.2f}".format(f_score))


def cv_evaluate(X,y):
    
    from sklearn import metrics
    from matplotlib import pyplot
    from sklearn.model_selection import KFold
    X, y = numpy.array(X), numpy.array(y)
    kf = KFold(5,shuffle=True)
    
    results = {}
    for algo in model:
        clf = model[algo]
        fold_counter = 0
        for train, test in kf.split(X):
            training_X, training_y = X[train], y[train]
            test_X, test_y = X[test], y[test]
            
            clf.fit(training_X,training_y)
            scores = clf.predict_proba(test_X)[:,-1]
            fpr, tpr, thresholds = metrics.roc_curve(test_y, scores)
            pyplot.semilogx(fpr,tpr,label="Fold number {0}".format(fold_counter))    
            fold_counter += 1
        
        # score=clf.score(test_X, test_y)   
        # results[algo] = score
        # print("%f %%" % (score*100))
        predictions=clf.predict(test_X)
        # print(algo + " F1-score: %.6f" % f1_score(test_y, predictions, average='weighted') )
        
        f_score=report(test_y, predictions)   
        print(algo, f_score)
    
    
def get_training_data(benign_path,malicious_path,hasher):
    def get_training_paths(directory):
        targets = []
        for path in os.listdir(directory):
            targets.append(os.path.join(directory,path))
        return targets
    malicious_paths = get_training_paths(malicious_path)
    benign_paths = get_training_paths(benign_path)
    X = [get_string_features(path,hasher) for path in malicious_paths + benign_paths]
    y = [1 for i in range(len(malicious_paths))] + [0 for i in range(len(benign_paths))]
    return X, y

parser = argparse.ArgumentParser("get windows object vectors for files")
parser.add_argument("--malware_paths",default=None,help="Path to malware training files")
parser.add_argument("--benignware_paths",default=None,help="Path to benignware training files")
parser.add_argument("--scan_file_path",default=None,help="File to scan")
parser.add_argument("--evaluate",default=False,action="store_true",help="Perform cross-validation")

args = parser.parse_args()

hasher = FeatureHasher(2000)
if args.malware_paths and args.benignware_paths and not args.evaluate:
    train_detector(args.benignware_paths,args.malware_paths,hasher)
elif args.scan_file_path:
    scan_file(args.scan_file_path)
elif args.malware_paths and args.benignware_paths and args.evaluate:
    X, y = get_training_data(args.benignware_paths,args.malware_paths,hasher)
    cv_evaluate(X,y)
else:
    print("No specified path to malware/benign files or no file to scan")
