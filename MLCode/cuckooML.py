import os
import sys
import pickle
import argparse
import requests
import time
import re
import numpy
from sklearn.feature_extraction import FeatureHasher
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score
from sklearn.model_selection import train_test_split

from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
import xgboost as xgb

model = {"DecisionTree": tree.DecisionTreeClassifier(max_depth=10),
            "RandomForest": RandomForestClassifier(n_estimators=64),
            "Adaboost": AdaBoostClassifier(n_estimators=50),
            "ExtremeGradientBoosting": xgb.XGBClassifier(random_state=123),
            "LogisticRegression": LogisticRegression()
        }



def getRaport(filename,hasher):
               
        REST_URL = " http://10.13.130.1:1337/tasks/create/file"
        SAMPLE_FILE = filename
        HEADERS = {"Authorization": "Bearer Xt76G_fPKiqdjNqbG4Adaw"}

        with open(SAMPLE_FILE, "rb") as sample:
            files = {"file": ("temp_file_name", sample)}
            r = requests.post(REST_URL, headers=HEADERS, files=files)

      
        task_id = r.json()["task_id"]
        REST_URL = " http://10.13.130.1:1337/tasks/view/"+str(task_id)
        r = requests.get(REST_URL, headers=HEADERS)
        
        while r.json()["task"]["status"]!="reported":
            time.sleep(10)
            r = requests.get(REST_URL, headers=HEADERS)
        
        REST_URL = " http://10.13.130.1:1337/tasks/report/"+str(task_id)
        r = requests.get(REST_URL, headers=HEADERS)  
        
        if r.status_code==200:
            result=[]
            if 'behavior' in r.json() and 'processes' in r.json()['behavior']:
                mylist = r.json()['behavior']['processes']
                
            
                for i in mylist:
                    result.append(i['process_name'])
                    result.append(i['process_path'])
                    
            else:
                result.append('')
                
            features = {}
            for k in result:
                features[k] = 1
            
            # hashing trick
            hashed_features = hasher.transform([features])
            hashed_features = hashed_features.todense()
            hashed_features = numpy.asarray(hashed_features)
            hashed_features = hashed_features[0]
            
            return hashed_features    
                
def scan_file(path):
    # scan a file to determine if it is malicious or benign
    if not os.path.exists("cuckoo.pkl"):
        print("It appears you haven't trained a detector yet!  Do this before scanning files.")
        sys.exit(1)
         
    with open("cuckoo.pkl", 'rb') as saved_detector:
        classifier, hasher = pickle.load(saved_detector)

    features = getRaport(path, hasher)
    result_proba = classifier.predict_proba([features])[:,1]   
    print(str(result_proba.tolist()[0]*100))
           

        
def train_detector(benign_path,malicious_path,hasher):
    # train the detector on the specified training data
    def get_training_paths(directory):
        targets = []
        for path in os.listdir(directory):
            targets.append(os.path.join(directory,path))
        return targets
    malicious_paths = get_training_paths(malicious_path)
    benign_paths = get_training_paths(benign_path)
    X=[]
    i=0
    j=0
    for path in malicious_paths: 
        X.append(getRaport(path, hasher))
        i+=1
    
    for path in benign_paths: 
        X.append(getRaport(path, hasher))
        j+=1
    
    y = [1 for k in range(i)] + [0 for i in range(j)]

    X_train, X_test, y_train, y_test = train_test_split(X, y ,test_size=0.2)
    results = {}
    for algo in model:
        clf = model[algo]
        clf.fit(X_train,y_train)
        score = clf.score(X_test,y_test)
        results[algo] = score
    
    winner = max(results, key=results.get)
    print(winner)
    pickle.dump((model[winner], hasher),open("cuckoo.pkl","wb"))

def report(y_test, y_pred):
    print("\tAccuracy Score: {0:.4f}".format(accuracy_score(y_test, y_pred)))
    tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
    tpr = tp / (tp + fn)
    fpr = fp / (fp + tn)
    pv = tp / (tp + fp)
    f_score = 2 * tpr * pv / (tpr + pv)
    print("\tTN:{0}\t\tFP:{1}\t\tFN:{2}\t\tTP:{3}".format(tn, fp, fn, tp))
    print("\tTPR:{0:.4f}\tFPR:{1:.4f}\tPV:{2:.4f}\tFSc:{3:.4f}".format(tpr, fpr, pv, f_score))


def cv_evaluate(X,y):
    # use cross-validation to evaluate our model
    from sklearn import metrics
    from matplotlib import pyplot
    from sklearn.model_selection import KFold
    X, y = numpy.array(X), numpy.array(y)
    kf = KFold(5,shuffle=True)
  
    print("Performante caracteristici cuckoo:\n")
    results = {}
    for algo in model:
        clf = model[algo]
        fold_counter = 0
        for train, test in kf.split(X):
            training_X, training_y = X[train], y[train]
            test_X, test_y = X[test], y[test]
            
            clf.fit(training_X,training_y)
            scores = clf.predict_proba(test_X)[:,-1]
            fpr, tpr, thresholds = metrics.roc_curve(test_y, scores)
            pyplot.semilogx(fpr,tpr,label="Fold number {0}".format(fold_counter))    
            fold_counter += 1
        
        # score=clf.score(test_X, test_y)   
        # results[algo] = score
        # print("%f %%" % (score*100))
        predictions=clf.predict(test_X)
        # print(algo + " F1-score: %.6f" % f1_score(test_y, predictions, average='weighted') )
        print("----------------------------------------------------")
        print(algo, "\n")
        report(test_y, predictions)
    

def get_training_data(benign_path,malicious_path,hasher):
    def get_training_paths(directory):
        targets = []
        for path in os.listdir(directory):
            targets.append(os.path.join(directory,path))
        return targets
    malicious_paths = get_training_paths(malicious_path)
    benign_paths = get_training_paths(benign_path)
    
    X=[]
    i=0
    j=0
    for path in malicious_paths: 
        X.append(getRaport(path,hasher))
        i+=1

    for path in benign_paths: 
        X.append(getRaport(path,hasher))
        j+=1
    
    y = [1 for k in range(i)] + [0 for i in range(j)]
       
    return X, y

parser = argparse.ArgumentParser("get windows object vectors for files")
parser.add_argument("--malware_paths",default=None,help="Path to malware training files")
parser.add_argument("--benignware_paths",default=None,help="Path to benignware training files")
parser.add_argument("--scan_file_path",default=None,help="File to scan")
parser.add_argument("--evaluate",default=False,action="store_true",help="Perform cross-validation")

args = parser.parse_args()
hasher = FeatureHasher(200)

if args.malware_paths and args.benignware_paths and not args.evaluate:
    train_detector(args.benignware_paths,args.malware_paths,hasher)
elif args.scan_file_path:
    scan_file(args.scan_file_path)
elif args.malware_paths and args.benignware_paths and args.evaluate:
    X, y = get_training_data(args.benignware_paths,args.malware_paths,hasher)
    cv_evaluate(X,y)
else:
    print("No specified path to malware/benign files or no file to scan")

        