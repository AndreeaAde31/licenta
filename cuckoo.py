import time
import requests


class Cuckoo():
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
            
    def analyze(self):
               
        REST_URL = " http://10.13.130.1:1337/tasks/create/file"
        SAMPLE_FILE = self.filename
        HEADERS = {"Authorization": "Bearer Xt76G_fPKiqdjNqbG4Adaw"}

        with open(SAMPLE_FILE, "rb") as sample:
            files = {"file": ("temp_file_name", sample)}
            r = requests.post(REST_URL, headers=HEADERS, files=files)

      
        task_id = r.json()["task_id"]
        REST_URL = " http://10.13.130.1:1337/tasks/view/"+str(task_id)
        r = requests.get(REST_URL, headers=HEADERS)
        
        while r.json()["task"]["status"]!="reported":
            time.sleep(1)
            r = requests.get(REST_URL, headers=HEADERS)
        
        REST_URL = " http://10.13.130.1:1337/tasks/report/"+str(task_id)
        r = requests.get(REST_URL, headers=HEADERS)  
        
        if r.status_code==200:
            mylist = r.json()['behavior']['processes']
            result_json={}
           
            for i in mylist:
                intermediar_json={}
                intermediar_json['image']=i['process_path']
                intermediar_json['pid']=i['pid']
                intermediar_json['ppid']=i['ppid']
                intermediar_json['CMD']=i['command_line']
                result_json[i['process_name']]=intermediar_json
            
            final={}
            final['Processes']=result_json
            
            result_json={}
            if 'summary' in r.json()['behavior']:
                mylist = r.json()['behavior']['summary']['file_opened']
                final['File Opened']=mylist
            
            if 'signatures' in r.json():
                result_json={}
                mylist = r.json()['signatures']
                for i in mylist:
                    intermediar_json={}
                    intermediar_json['severity']=i['severity']
                    intermediar_json['description']=i['description']
                    result_json[i['name']]=intermediar_json
                
                final['Signatures']=result_json
            
            return final
             
               
    