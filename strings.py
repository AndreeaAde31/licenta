from cv import IAnalyzer


class Strings(IAnalyzer):
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
            
    
    def analyze(self):
        cmd="strings -a '" + self.filename + "' "
        output=self.run(cmd)
        lista=list(output.split("\r\n"))
        str1='\n'.join(lista[5:])  
        ascii=str1.encode()                
        
        cmd2="strings -u '" + self.filename + "' "
        output2=self.run(cmd2)
        lista2=list(output2.split("\r\n"))
        str2='\n'.join(lista2[5:])            
        unicode=str2.encode()        
        with open(".\\frontend\\src\\StringsFile\\Ascii.bin", "wb") as binary_file:
          binary_file.write(ascii)
     
        with open(".\\frontend\\src\\StringsFile\\Unicode.bin", "wb") as binary_file:
          binary_file.write(unicode)        
                
# tool=Strings("C:\\Users\\Ade\\Desktop\\Licenta\\b284b6cc53f3fd14bbe580e7f2313d2647c53158069c7249e86edb4da749e408.exe")
# tool.analyze()              