from cv import IAnalyzer

class Trid(IAnalyzer):
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
    
    def analyze(self):
        cmd="trid '" + self.filename +"' "
        output=self.run(cmd)
        lista=list(output.split("\n"))
        lines=lista[6:]
        result=[]
        for i in lines[:-1]:
            word=i.split()[:-2]
            result.append(" ".join(word))
   
        return result
                          