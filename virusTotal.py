from cv import IAnalyzer
import json
import requests



class VirusTotal(IAnalyzer):
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
            
    def analyze(self):
               
        header={'x-apikey':'8e1db9b78b015bf3022cccdfc95675a5cc373d6966a4a28887c0b2657ef1bc76'}
        url='https://www.virustotal.com/api/v3/files/'+self.filename
        r = requests.get(url,headers=header)
        res = json.loads(r.text)
        
        sandbox_json={}
        x=0
        y=0
        final={}
               
        if 'sandbox_verdicts' in res['data']['attributes']:
            sandbox_req=res['data']['attributes']['sandbox_verdicts']
            
            for key in sandbox_req:
                
                intermediar_json={}
                intermediar_json['category']=sandbox_req[key]['category']
                            
                if intermediar_json['category']=='malicious':
                    x+=1
                else: y+=1
                
                intermediar_json['classification']=sandbox_req[key]['malware_classification'][0]
                sandbox_json[key]=intermediar_json
                
            final['Sandboxes']=sandbox_json
        
        engine_json={} 
        engine_req=res['data']['attributes']['last_analysis_results']
        
        for key in engine_req:
            
            intermediar_json={}
            intermediar_json['category']=engine_req[key]['category']
            if engine_req[key]['category']=="undetected":
                intermediar_json['result']=''
                y+=1
            else:
                intermediar_json['result']=engine_req[key]['result']
                x+=1
                
            engine_json[key]=intermediar_json
            
        final['Malicious']=x
        final['Undetected']=y
        final['Engines']=engine_json
        
        return final
        
    