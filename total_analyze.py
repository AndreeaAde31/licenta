#in clasa asta apelez cate o instanta ptr fiecare tool
import os
from code import interact
from createDB import fetchResults, createMetadataObj, getMetadataID, createAnalysisObj
import json
from getFileHash import GetFileHash
from ssdeep import SSDeep
from trid import Trid
from strings import Strings
from diec import Diec
from yara import Yara
from virusTotal import VirusTotal
from metadefender import Metadefender
from cuckoo import Cuckoo
import sys
sys.path.insert(1, './ML Code/')

from MLCode.machineLearning import MachineLearning

class TotalAnalyzer:
  def __init__(self, filename, id=0):
    self.filename = filename
    self.id = id

  def analyze(self):
    
    hasher=GetFileHash(self.filename)
    
    result=hasher.analyze()
    #vad daca exista in Metadata o intrare cu Hash=result
    exist=fetchResults("SELECT ID from Metadata WHERE Hash='"+result+"' ;")
    
    if exist == () : 
      myjson={}
      myjson["Hash"]=result
      json_data = json.dumps(myjson)
      createMetadataObj(result)
      id=getMetadataID(result)    
      self.id=id
      createAnalysisObj(id, json_data, "Get-FileHash")
      print(json_data, "\n")
          
      ssdeep=SSDeep(self.filename, id)
      fuzzy = ssdeep.analyze()
      myjson2={}
      myjson2["Fuzzy Hash"]=fuzzy
      json_data2 = json.dumps(myjson2)
      createAnalysisObj(id, json_data2, "SSDeep")
      print(json_data2, "\n")
      
      trid=Trid(self.filename)
      types = trid.analyze()
      myjson3={}
      myjson3["Types"]=types
      json_data3 = json.dumps(myjson3)
      createAnalysisObj(id, json_data3, "Trid")
      print(json_data3, "\n")
      
      strings=Strings(self.filename)
      strings.analyze()
          
      diec=Diec(self.filename)
      report = diec.analyze()
      json_data6 = json.dumps(report)
      createAnalysisObj(id, json_data6, "Diec")
      print(json_data6, "\n")
      
      yara=Yara(self.filename)
      sig = yara.analyze()
      json_data7 = json.dumps(sig)
      createAnalysisObj(id, json_data7, "Yara")
      print(json_data7, "\n")
     
      virusTotal=VirusTotal(result)
      detected=virusTotal.analyze()
      json_data8=json.dumps(detected)
      createAnalysisObj(id, json_data8, "VirusTotal")
      print(json_data8, "\n")
      
      metadender=Metadefender(result)
      meta=metadender.analyze()
      json_data9=json.dumps(meta)
      createAnalysisObj(id, json_data9, "Metadefender")
      print(json_data9, "\n")
      
      cuckoo=Cuckoo(self.filename)
      behavior=cuckoo.analyze()
      print(behavior)
      json_data10=json.dumps(behavior)
      createAnalysisObj(id, json_data10, "Cuckoo")
      print(json_data10, "\n")
      
      ml=MachineLearning(self.filename)
      probability=ml.analyze()
      json_data11=json.dumps(probability)
      createAnalysisObj(id, json_data11, "MachineLearning")
      print(json_data11, "\n")
      
    else:
        self.id=exist[0][0]
     
  def returnReport(self):
    #select * from analysis where MetadataId=self.id  
    
    analysis=fetchResults("SELECT * from Analysis WHERE MetadataID="+str(self.id)+" ;")
    lista=[]
   
    for i in analysis: 
      intermediar={}
      intermediar['Tool']=i[3]
      intermediar['Result']=i[2]
      lista.append(intermediar)
    
    analysis=fetchResults("SELECT * from Performance ;")
    for i in analysis: 
      intermediar={}
      intermediar['DetectorType']=i[0]
      intermediar['Score']=i[1]
      intermediar['Script']=i[2]
      lista.append(intermediar)
    
    return lista
    
  def delete(self):
    os.remove(self.filename)  
      
# tool=TotalAnalyzer("C:\\Users\\Ade\\Desktop\\Licenta\\Backdoor.Win32.Agent.afxs_ed7c\\Backdoor.Win32.Agent.afxs_ed7c.exe")
# tool.analyze()    
# print(tool.returnReport())