from cv import IAnalyzer
import json

class Diec(IAnalyzer):
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
            
    
    def analyze(self):
        cmd="diec -j -e '" + self.filename + "' "
        output=self.run(cmd)
        
        json_object = json.loads(output) #dictionary
        cmd2="diec -j '" + self.filename + "' "
        output2=self.run(cmd2)
        json_object2 = json.loads(output2) #dictionary
        
        for i in json_object["records"]:
            if "'" in i["name"]:
                i["name"]=i["name"].replace("'","")
                   
        summary, sections, detects ={}, {}, {}
        summary["Status"]=json_object["status"]
        summary["Entropy"]=json_object["total"]
        sections["Sections"]=json_object["records"]
        detects["Packer Detects"]= json_object2["detects"]    
        summary["Arhitecture"]=json_object2["arch"]    
        summary["Endianess"]=json_object2["endianess"]   
        summary["BitMode"]=json_object2["mode"] 
        summary["FileType"]=json_object2["filetype"]    
        
        finalData={}
        finalData["Summary"]=summary
        finalData["Sections"]=sections
        finalData["Detects"]=detects
        res=finalData["Sections"]["Sections"]
            
        return finalData
       

# tool=Diec("C:\\Users\\Ade\\Desktop\\Licenta\\Proiect\\uploads\\Ransomeware.exe")
# tool.analyze()  