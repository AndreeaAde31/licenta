import hashlib


class GetFileHash():
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
            
    
    def analyze(self):
        
        sha256_hash = hashlib.sha256()
        with open(self.filename,"rb") as f:
            # Read and update hash string value in blocks of 4K
            for byte_block in iter(lambda: f.read(4096),b""):
                sha256_hash.update(byte_block)
            
            return sha256_hash.hexdigest()

        

# tool=GetFileHash("C:\\Users\\Ade\\Desktop\\Licenta\\Proiect\\uploads\\b284b6cc53f3fd14bbe580e7f2313d2647c53158069c7249e86edb4da749e408.exe")
# tool.analyze()