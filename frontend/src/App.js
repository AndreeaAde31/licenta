import React, { useState, useEffect } from 'react';
import './App.css';

import Forms from './Forms'; 

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <div className='container'>

      <Router>
        
        <Switch>
          <Route exact path='/' component={Forms} />
          {/* <Route path="/:id" component={ShowPage} /> */}
        </Switch>

      </Router>

    </div>
  )
}

export default App;