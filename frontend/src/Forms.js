import React, { useState } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import { TabsPattern } from './Components/TabsPattern';
import axios from 'axios';

const Forms = () => {
  const [file, setFile] = useState('');
  const [filename, setFilename] = useState('');
  const [uploadedFile, setUploadedFile] = useState({});
  const [message, setMessage] = useState('');
  const [showSpinner, setShowSpinner] = useState(false);

  const onChange = e => {
    setFile(e.target.files[0]);
    setFilename(e.target.files[0].name);
  };

  const onSubmit = e => {

    e.preventDefault();
    const formData = new FormData();
    formData.append('file', file);

    axios.post('/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
     
    }).then(res => {
      const { fileName, filePath } = res.data.message;
      setUploadedFile({ fileName, filePath });
      setMessage(res.data.message);
      setShowSpinner(false);
    }).catch( e => console.log(e));
    

    setShowSpinner(true);
  };
  

  return (

    <div>
      {showSpinner ? <CircularProgress /> : message ? <TabsPattern userInput={message} /> :

        <div className="drag-area">

          <div className='icon'>
            <i className='fas fa-cloud-upload-alt' />
          </div>
          <h1>Drag & Drop to Upload File</h1>
          <h2>OR</h2>

          <form onSubmit={onSubmit}>
            <div>
              <input
                type='file'
                className='custom-file-input'
                id='customFile'
                name='file'
                onChange={onChange}
              />

            </div>
            <br />


            <input
              type='submit'
              value='Upload'
              className='btn btn-primary btn-block mt-4'
            />
          </form>


          {uploadedFile ? (
            <div className='row mt-5'>
              <div className='col-md-6 m-auto'>
                <h3 className='text-center'>{uploadedFile.fileName}</h3>
                <img style={{ width: '100%' }} src={uploadedFile.filePath} alt='' />
              </div>
            </div>
          ) : null}

        </div>}


    </div>
  );
};

export default Forms;