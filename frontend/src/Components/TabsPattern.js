import React, {useState} from 'react';
import Details from '../Pages/Details';
import General from '../Pages/General';
import MachineLearning from '../Pages/MachineLearning';
import Metadefender from '../Pages/Metadefender';
import VirusTotal from '../Pages/VirusTotal';
import Yara from '../Pages/Yara';

import './TabsPattern.css'

export function TabsPattern (props) {
   
  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  var myObject = JSON.parse(props.userInput[5].Result);
  var procentVT=(myObject.Malicious*100)/(myObject.Malicious+myObject.Undetected);
  var procentYara=0;
  if ( Object.keys(props.userInput[4].Result).length > 0 )
    procentYara=100
  var procentML=JSON.parse(props.userInput[8].Result).Probability;

 
  return (
    <div className="containe">
      <div className="bloc-tabs">
        <button
          className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(1)}
        >
          General
        </button>
        <button
          className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(2)}
        >
          VirusTotal
        </button>
        <button
          className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(3)}
        >
          Metadefender
        </button>
        <button
          className={toggleState === 4 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(4)}
        >
          Yara
        </button>
        <button
          className={toggleState === 5 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(5)}
        >
          Machine Learning
        </button>
        <button
          className={toggleState === 6 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(6)}
        >
          Details
        </button>
      </div>

      <div className="content-tabs">
        <div
          className={toggleState === 1 ? "content  active-content" : "content"}
        >
          <h2>Report</h2>
          <hr />
          <General procentVT={procentVT} procentYara={procentYara} procentML={procentML} getFileHash={props.userInput[0].Result} ssdeep={props.userInput[1].Result} />
        </div>

        <div
          className={toggleState === 2 ? "content  active-content" : "content"}
        >
         
          <VirusTotal virustotal={props.userInput[5].Result} />
        </div>

        <div
          className={toggleState === 3 ? "content  active-content" : "content"}
        >
          
          <Metadefender metadefender={props.userInput[6].Result} />
        </div>

        <div
          className={toggleState === 4 ? "content  active-content" : "content"}
        >
         
          <Yara yara={props.userInput[4].Result} />
        </div>

        <div
          className={toggleState === 5 ? "content  active-content" : "content"}
        >
          
          <MachineLearning ml={props.userInput[8].Result} 

                          strings1={props.userInput[9].DetectorType}
                          score1={props.userInput[9].Score}
                          script1={props.userInput[9].Script}

                          strings2={props.userInput[10].DetectorType}
                          score2={props.userInput[10].Score}
                          script2={props.userInput[10].Script}

                          strings3={props.userInput[11].DetectorType}
                          score3={props.userInput[11].Score}
                          script3={props.userInput[11].Script}

                          strings4={props.userInput[12].DetectorType}
                          score4={props.userInput[12].Score}
                          script4={props.userInput[12].Script}

                          strings5={props.userInput[13].DetectorType}
                          score5={props.userInput[13].Score}
                          script5={props.userInput[13].Script}

                          header54_1={props.userInput[14].DetectorType}
                          score6={props.userInput[14].Score}
                          script6={props.userInput[14].Script}

                          header54_2={props.userInput[15].DetectorType}
                          score7={props.userInput[15].Score}
                          script7={props.userInput[15].Script}

                          header54_3={props.userInput[16].DetectorType}
                          score8={props.userInput[16].Score}
                          script8={props.userInput[16].Script}

                          header54_4={props.userInput[17].DetectorType}
                          score9={props.userInput[17].Score}
                          script9={props.userInput[17].Script}

                          header54_5={props.userInput[18].DetectorType}
                          score10={props.userInput[18].Score}
                          script10={props.userInput[18].Script}

                          headerPCA_1={props.userInput[19].DetectorType}
                          score11={props.userInput[19].Score}
                          script11={props.userInput[19].Script}

                          headerPCA_2={props.userInput[20].DetectorType}
                          score12={props.userInput[20].Score}
                          script12={props.userInput[20].Script}

                          headerPCA_3={props.userInput[21].DetectorType}
                          score13={props.userInput[21].Score}
                          script13={props.userInput[21].Script}

                          headerPCA_4={props.userInput[22].DetectorType}
                          score14={props.userInput[22].Score}
                          script14={props.userInput[22].Script}

                          headerPCA_5={props.userInput[23].DetectorType}
                          score15={props.userInput[23].Score}
                          script15={props.userInput[23].Script}

                          
                          />
        </div>

        <div
          className={toggleState === 6 ? "content  active-content" : "content"}
        >
          
          <Details diec={props.userInput[3].Result} trid={props.userInput[2].Result} cuckoo={props.userInput[7].Result} />
        </div>

      </div>
    </div>
  );

}