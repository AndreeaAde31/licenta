import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableContainer from '@mui/material/TableContainer';
import Paper from "@material-ui/core/Paper";
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';


const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
}));

export default function Metadefender (props) {
   
  var metadefender=JSON.parse(props.metadefender);
  var header=metadefender.Header;
  var imported=metadefender['Imported Dlls'];
  
  const sample = [
    { name: "Machine Type", detail: [header.machine_type] },
    { name: "Compilation Time", detail: [header.compilation_time] },
    { name: "Number of Sections", detail: [header.number_of_sections] },
    { name: "Number of Symbols", detail: [header.number_of_symbols] },
    { name: "Characteristics", detail: header.characteristics }
  ];
  
  const classes = useStyles();

  //cod ptr imported tables
  var rows=[];
  var nume;
  var no;
  var parent;
  var length=Object.keys(imported).length;
  
  for (var j=0; j< length; j++){
    nume=imported[j].name;
    parent=nume;
    no=imported[j].functions.length;
   
    var intermediar=[];
    
    for (var i = 0; i < no; i++) {
      nume=imported[j].functions[i];
      intermediar.push({name: nume, parent: parent});
    }

    rows.push(intermediar);
   
  }


  return (


    <Box sx={{ flexGrow: 1 }} >
      <Grid container spacing={2}>

        <Grid item xs={12}>
          <Item>
            <Paper className={classes.root}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Header</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {sample.map(item => (
                    <React.Fragment>
                      <TableRow>
                        <TableCell rowSpan={item.detail.length + 1}>
                          {item.name}
                        </TableCell>
                      </TableRow>
                      {item.detail.map(detail => (
                        <TableRow>
                          <TableCell>{detail}</TableCell>
                        </TableRow>
                      ))}
                    </React.Fragment>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </Item>
        </Grid>

        {rows.map((row) => (
          <Grid item xs>
            <Item>  
            <div >
                <TableContainer component={Paper}>
                  <Table aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell sx={{
                            backgroundColor: "#2986cc",
                            borderBottom: "2px solid black",
                            "& th": {
                              fontSize: "1.25rem",
                              color: "rgba(96, 96, 96)"
                            }
                          }}>
                          {row[0].parent}
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {row.map((row2) => (
                        <TableRow
                          key={row2.name}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell component="th" scope="row">
                            {row2.name}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>

            </div>

            </Item>
            </Grid>
          ))
          }

      </Grid>
    </Box>

  );

}