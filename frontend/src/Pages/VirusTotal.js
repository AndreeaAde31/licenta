import React from 'react';
import Chart from "react-apexcharts";
import './General.css';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';


import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';



const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));



export default function VirusTotal (props) {

  var mal=JSON.parse(props.virustotal).Malicious;
  var undetected=JSON.parse(props.virustotal).Undetected;
  var sandboxes=JSON.parse(props.virustotal).Sandboxes;
  var engines=JSON.parse(props.virustotal).Engines;

  var rows = [];
  var length=Object.keys(sandboxes).length;

  // var nume=Object.keys(sandboxes)[0];
  // console.log(sandboxes[nume].category);

  var nume;
  var clas;
  var cat;
  
  for (var i = 0; i < length; i++) {
    nume=Object.keys(sandboxes)[i];
    cat=sandboxes[nume].category;
    clas=sandboxes[nume].classification;
    rows.push({name: nume, category: cat, classification: clas});
  }

  var rows2 = [];
  length=Object.keys(engines).length;

  var nume2;
  var result;
  var cat2;
  
  for (var i = 0; i < length; i++) {
    nume2=Object.keys(engines)[i];
    result=engines[nume2].result;
    cat2=engines[nume2].category;
    rows2.push({name: nume2, result:result, category: cat2});
  }
  

  const options = {
    series: [mal, undetected],
    labels: ["Malicious", "Undetected"],
    colors: ['#9c3d84', '#ffcb2d'],
    plotOptions: {
      pie: {
        expandOnClick: false,
        donut: {
          size: "60px", 
          labels: {
            show: false,
            showAlways: false
          }
        }
      }
    }
  }; 

  const series = [mal, undetected];

  return (
    <Box sx={{ flexGrow: 1 }} >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Item>
          <div className='vt'>
              <h1 >         
                {mal} of {mal+undetected} sources indicated the file is a malware
              </h1>
              <br/>
              <Chart
                options={options}
                series={series}
                type="donut"
                width="100%"
                height={300}
              />
            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
          <div className='info'>
              <h3 >         
              Sandbox Verdicts
              </h3>
             
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell align="right">Category</TableCell>
                      <TableCell align="right">Classification</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map((row) => (
                      <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {row.name}
                        </TableCell>
                        <TableCell align="right">{row.category}</TableCell>
                        <TableCell align="right">{row.classification}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>

            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
         
          <div className='info'>
              <h3 >         
              Engines
              </h3>
              
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Engine</TableCell>
                      <TableCell align="right">Result</TableCell>
                      <TableCell align="right">Category</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows2.map((rows2) => (
                      <TableRow
                        key={rows2.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {rows2.name}
                        </TableCell>
                        <TableCell align="right">{rows2.result}</TableCell>
                        <TableCell align="right">{rows2.category}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>

            </div>
          </Item>
        </Grid>
        
      </Grid>
    </Box>
  );

}