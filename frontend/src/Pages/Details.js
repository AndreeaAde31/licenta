import React, {useState} from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableContainer from '@mui/material/TableContainer';
import Paper from "@material-ui/core/Paper";
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Chart from "react-apexcharts";
import './General.css';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(1),
    overflowX: "auto"
  },
  table: {
    minWidth: 50
  }
}));


export default function Details (props) {
  
  var diec=JSON.parse(props.diec);

  //cod pentru diec (3 tabele)
  var summary=diec.Summary;
  const sample1 = [
    { name: "Status", detail: [summary.Status] },
    { name: "Entropy", detail: [parseFloat(summary.Entropy).toFixed(2)] },
    { name: "Arhitecture", detail: [summary.Arhitecture] },
    { name: "Endianess", detail: [summary.Endianess] },
    { name: "BitMode", detail: [summary.BitMode] },
    { name: "FileType", detail: [summary.FileType] }
  ];
 
  const classes = useStyles();

  var sections=diec.Sections.Sections;
  
  var packer=diec.Detects['Packer Detects'];

  //cod pentru trid (doughchart)
  var types=JSON.parse(props.trid).Types;
  var length=types.length;

  var procent;
  var label;
  var list;
  
  var series=[];
  var labels=[];

  for (var i = 0; i < length; i++) {
    list=types[i].split("%");
    procent=parseFloat(list[0]);
    label=list[1];
    series.push(procent);
    labels.push(label);
  }
  

  const options = {
    series: series,
    labels: labels,
    //colors: ['#9c3d84', '#ffcb2d'],
    plotOptions: {
      pie: {
        expandOnClick: false,
        donut: {
          size: "60px", 
          labels: {
            show: false,
            showAlways: false
          }
        }
      }
    }
  }; 
  

  //cod pentru cuckoo (Processes, File Opened, Signatures-3 tabele)
  var str=props.cuckoo;
  
  var imagepath = str.replace(/\\/g, "\\\\");
  imagepath=imagepath.replace(/""/g, '"');
  imagepath=imagepath.replace(/" "/g, '"');
  imagepath=imagepath.replace(/" C:\\[a-zA-Z\\.-]+"/g, '"');
  imagepath=imagepath.replace(/" -[a-zA-Z\\.-]+"/g, '"');
  let regex = /"schtasks\.exe" \/create \/f \/tn "TCP Service Task" \/xml/i;
  imagepath=imagepath.replace(regex, '"');
  imagepath=imagepath.replace(/" "/g, '"');
  console.log(imagepath);
  var cuckoo=JSON.parse(imagepath);

  var processes=cuckoo.Processes;
  var length2=Object.keys(processes).length;
  // console.log(typeof props.cuckoo);
  
  var nume;
  var image;
  var pid;
  var ppid;
  var cmd;
  var rows2=[];

  for (var i = 0; i < length2; i++) {
    nume=Object.keys(processes)[i];
    image=processes[nume].image;
    pid=processes[nume].pid;
    ppid=processes[nume].ppid;
    cmd=processes[nume].CMD;
    rows2.push({name: nume, image: image, pid: pid, ppid: ppid, cmd: cmd});
  }
  
  var signatures=cuckoo.Signatures;
  var length3=Object.keys(signatures).length;

  var severity;
  var description;
  var rows3=[];

  for (var i = 0; i < length3; i++) {
    nume=Object.keys(signatures)[i];
    severity=signatures[nume].severity;
    description=signatures[nume].description;
    rows3.push({name: nume, severity: severity, description: description});
  }
  var exists=0;

  if (('File Opened' in cuckoo)==true)
  {
    exists=1;
    var files=cuckoo['File Opened'];
    var length4=files.length;
    var rows4=[];

    for (var i = 0; i < length4; i++){
      if(files[i]!=='C:\\' && files[i]!=='C:\\Users')
        rows4.push({name: files[i]});
    }
    console.log(rows4);
  }
  


  return (
   
    <Box sx={{ flexGrow: 1 }} >
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Item>
          <Paper className={classes.root}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Summary</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {sample1.map(item => (
                    <React.Fragment>
                      <TableRow>
                        <TableCell rowSpan={item.detail.length + 1}>
                          {item.name}
                        </TableCell>
                      </TableRow>
                      {item.detail.map(detail => (
                        <TableRow>
                          <TableCell>{detail}</TableCell>
                        </TableRow>
                      ))}
                    </React.Fragment>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          </Item>
        </Grid>

        
        <Grid item xs={8}>
          <Item>
          <div className='details'>
              <h1>         
              Packers
              </h1>
             
              <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell >Name</TableCell>
                      <TableCell align="right">Options</TableCell>
                      <TableCell align="right">Type</TableCell>
                      <TableCell align="right">Version</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {packer.map((row) => (
                      <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {row.name}
                        </TableCell>
                        <TableCell align="right">{row.options}</TableCell>
                        <TableCell align="right">{row.type}</TableCell>
                        <TableCell align="right">{row.version}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
            <div className='cuckoo'>
              <h3 >         
              Entropy per Sections
              </h3>
             
              <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell >Name</TableCell>
                      <TableCell align="right">Entropy</TableCell>
                      <TableCell align="right">Size</TableCell>
                      <TableCell align="right">Status</TableCell>
                      <TableCell align="right">Offset</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {sections.map((row) => (
                      <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {row.name}
                        </TableCell>
                        <TableCell align="right">{parseFloat(row.entropy).toFixed(2)}</TableCell>
                        <TableCell align="right">{row.size}</TableCell>
                        <TableCell align="right">{row.status}</TableCell>
                        <TableCell align="right">{row.offset}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>

            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
            <div className='cuckoo'>
              <h3 >         
              Trid Result
              </h3>
             <br/>
             <Chart
                options={options}
                series={series}
                type="donut"
                width="100%"
                height={300}
              />
            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
            <div className='cuckoo'>
            <h3 >         
              Processes
            </h3>
             <br/>
             <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell align="right">Image</TableCell>
                      <TableCell align="right">PID</TableCell>
                      <TableCell align="right">PPID</TableCell>
                      <TableCell align="right">CMD</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows2.map((row) => (
                      <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {row.name}
                        </TableCell>
                        <TableCell align="right">{row.image}</TableCell>
                        <TableCell align="right">{row.pid}</TableCell>
                        <TableCell align="right">{row.ppid}</TableCell>
                        <TableCell align="right">{row.cmd}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
            <div className='cuckoo'>
            <h3 >         
              Signatures
            </h3>
             <br/>
             <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell >Name</TableCell>
                      <TableCell align="right">N</TableCell>
                      <TableCell align="right">Description</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows3.map((row) => (
                      <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {row.name}
                        </TableCell>
                        <TableCell align="right">{row.severity}</TableCell>
                        <TableCell align="right">{row.description}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </Item>
        </Grid>
        
        {exists ? (
            <Grid item xs={12}>
            <Item>
              <div className='cuckoo'>
              <h3 >         
                Files Opened
              </h3>
               <br/>
               <TableContainer component={Paper}>
                  <Table  aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell >Filename</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {rows4.map((row) => (
                        <TableRow
                          key={row.name}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell component="th" scope="row">
                            {row.name}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
            </Item>
          </Grid>
          ) : null}
        
      </Grid>
    </Box>

  );

}