import React, {useState} from 'react';
import Chart from "react-apexcharts";
import './General.css';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';


import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';


const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


export default function MachineLearning (props) {
  
  var ml=JSON.parse(props.ml)
  var probability=ml.Probability;
  var strings=ml['Extracted strings'];

  var rows2 = [];
  rows2.push({name: props.strings1, string: props.score1, header: props.score6, pca: props.score11});
  rows2.push({name: props.strings2, string: props.score2, header: props.score7, pca: props.score12});
  rows2.push({name: props.strings3, string: props.score3, header: props.score8, pca: props.score13});
  rows2.push({name: props.strings4, string: props.score4, header: props.score9, pca: props.score14});
  rows2.push({name: props.strings5, string: props.score5, header: props.score10, pca: props.score15});


  return (
    <Box sx={{ flexGrow: 1 }} >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Item>
          <div  className='vt'>
              <h1>         
                Extracted {strings} strings 
              </h1>
            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
          <div className='vt'>
              <h1>         
              The file is malicious with a probability of {parseFloat(probability).toFixed(2)}% 
              </h1>
            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
         
          <div className='info'>
              <h3 >         
              Performance comparison of detectors
              </h3>
              
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Detector Type</TableCell>
                      <TableCell align="right">F1 Score Strings</TableCell>
                      <TableCell align="right">F1 Score 54-PE Header</TableCell>
                      <TableCell align="right">F1 Score PCA-PE Header</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows2.map((rows2) => (
                      <TableRow
                        key={rows2.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {rows2.name}
                        </TableCell>
                        <TableCell align="right">{rows2.string}</TableCell>
                        <TableCell align="right">{rows2.header}</TableCell>
                        <TableCell align="right">{rows2.pca}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>

            </div>
          </Item>
        </Grid>
        
      </Grid>
    </Box>
  );

}