import React, {useState} from 'react';
import Chart from "react-apexcharts";
import './General.css';
import Ascii from '../StringsFile/Ascii.bin';
import Unicode from '../StringsFile/Unicode.bin';

import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));



export default function General (props) {
   
  var sha256=JSON.parse(props.getFileHash).Hash;
  var ssdeep=JSON.parse(props.ssdeep)["Fuzzy Hash"];



  var procent=(props.procentVT+props.procentYara+props.procentML)/3;
  procent=parseFloat(procent);
  var undetected=parseFloat(100-procent);

  const options = {
    series: [procent, undetected],
    labels: ["Malicious", "Undetected"],
    plotOptions: {
      pie: {
        expandOnClick: false,
        donut: {
          size: "60px", 
          labels: {
            show: false,
            showAlways: false
          }
        }
      }
    }
  };

  const series = [procent, undetected];

  return (
    <Box sx={{ flexGrow: 1 }} >
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Item>
            <div className="info">
              <h2 >         
                {procent.toFixed(1)}% chances that this file is a malware
              </h2>
              <br/>
              <Chart
                options={options}
                series={series}
                type="donut"
                width="100%"
                height={300}
              />
            </div>
          </Item>
        </Grid>
        <Grid item xs={6}>
          <Item>
            <div className="info">
                <h1>
                  SHA-256
                </h1>
                <h2>
                {sha256}
                </h2>
                
                <h1>
                  SSDEEP
                </h1>
                <h2>
                {ssdeep}
                </h2>

                <a href ={Ascii} download={Ascii}>
                <button class="dbutton"><i class="fa fa-download"></i>Ascii</button>
                </a> 

                <br/>
                <br/>
                <a href ={Unicode} download={Unicode}>
                <button class="dbutton"><i class="fa fa-download"></i>Unicode</button>
                </a>

            </div>
          </Item>
        </Grid>

        <Grid item xs={12}>
          <Item>
          <div className="info">
            {/* <h2>
              Malitious Score Per Tool
            </h2> */}

          <Chart
            type="bar"
            width="100%"
            height={300}
            series={[
              {
                data: [parseInt(props.procentVT), parseInt(props.procentYara), parseInt(props.procentML)]
              },
            ]}
            options={{
              title: {
                text: "Malicious Score Per Tool",
                style: { fontSize: 35, fontFamily: 'Charis SIL, serif', color: '#000'},
              },

              colors: ['#9878a1'],
              theme: { mode: "light" },

              xaxis: {
                tickPlacement: "on",
                categories: [
                 "VirusTotal",
                 "Yara",
                 "Machine Learning"
                ]                
              },

              yaxis: {
                labels: {
                  formatter: (val) => {
                    return `${val}`;
                  },
                  style: { fontSize: "15", colors: ["#6b6b6b"] },
                }
              },

              legend: {
                show: true,
                position: "right",
              },

              dataLabels: {
                formatter: (val) => {
                  return `${val}`;
                },
                style: {
                  colors: ["#f4f4f4"],
                  fontSize: 15,
                },
              },
            }}
        ></Chart> 


          </div>

          </Item>
        </Grid>
        
      </Grid>
    </Box>
  

    
  );

}