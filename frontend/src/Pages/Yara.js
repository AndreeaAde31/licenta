import React, {useState} from 'react';
import Chart from "react-apexcharts";
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import './General.css';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary
  
}));

export default function Yara (props) {
   
  var yara=JSON.parse(props.yara);
  var length=Object.keys(yara).length;
  
  var categories=[];
  var data=[];
  
  var nume;
  var no;
  
  for (var i = 0; i < length; i++) {
    nume=Object.keys(yara)[i];
    no=yara[nume].Number;
    categories.push(nume);
    data.push(no);
  }

  var rows=[];
  var nume;
  var parent;
  
  for (var j=0; j< length; j++){
    nume=Object.keys(yara)[j];
    parent=nume;
    no=yara[nume].Number;
   
    var intermediar=[];
    var obj=yara[nume].Rules;

    for (var i = 0; i < no; i++) {
      nume=obj[i];
      intermediar.push({name: nume, parent: parent});
    }

    rows.push(intermediar);
  }
  
  return (
      <Box >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Item>
              <div className="vt">
               
                <br/>
                <Chart
                  type="bar"
                  width="100%"
                  height={300}
                  series={[
                    {
                      data: data
                    },
                  ]}
                  options={{
                    title: {
                      text: "Yara Results",
                      style: { fontSize: 35, fontFamily: 'Charis SIL, serif', color: '#000'}
                    },

                    colors: ['#9c3d84'],
                    theme: { mode: "light" },

                    xaxis: {
                      tickPlacement: "on",
                      categories: categories    
                    },

                    yaxis: {
                      labels: {
                        formatter: (val) => {
                          return `${val}`;
                        },
                        style: { fontSize: "15", colors: ["#6b6b6b"] },
                      }
                    },

                    legend: {
                      show: true,
                      position: "right",
                    },

                    dataLabels: {
                      formatter: (val) => {
                        return `${val}`;
                      },
                      style: {
                        colors: ["#f4f4f4"],
                        fontSize: 15,
                      },
                    },
                  }}
              ></Chart> 

              </div>
            </Item>
          </Grid>

          <Grid item xs={12}>
            <Item>
              <div className="yara">
                  <h1>
                    Yara Details
                  </h1>
                  
              </div>
            </Item>
          </Grid>

          {rows.map((row) => (
          <Grid item xs>
            <Item>  
            <div >
               
                <TableContainer component={Paper}>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell sx={{
                            backgroundColor: "#2986cc",
                            borderBottom: "2px solid black",
                            "& th": {
                              fontSize: "1.25rem",
                              color: "rgba(96, 96, 96)"
                            }
                          }}>
                          {row[0].parent}
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {row.map((row2) => (
                        <TableRow
                          key={row2.name}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell component="th" scope="row">
                            {row2.name}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>

            </div>

            </Item>
            </Grid>
          ))
          }

        </Grid>
      </Box>
    
  
    )
  
}