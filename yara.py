from cv import IAnalyzer
import glob
import threading

jsonData={}


class Yara(IAnalyzer):
    
    def __init__(self, filename):
        super().__init__()
        self.filename=filename
        
    def getReport(self, lista):
        
        start="rules-master\\"
        end="_index.yar"
        
        for i in lista:
            cmd="yara64 -w " + i + " '" + self.filename + "' "
            output=self.run(cmd)
            if output != "":
                lista=list(output.split("\r\n"))
                strings=[]
                strings[:]=[x for x in lista if x]
                new_strings = [x.replace(self.filename, '').replace(' ', '') for x in strings]
                value={}
                value["Rules"]=new_strings
                value["Number"]=len(new_strings)            
                result = i[len(start):-len(end)]
                jsonData[result]=value   
             
            
    def analyze(self):
        
        #extrag fisierele cu .yar la final din folderul rules-master
        mylist = [f for f in glob.glob("rules-master\\*.yar")]
        
        t1 = threading.Thread(target=self.getReport(mylist[0:3]), name='t1')
        t2 = threading.Thread(target=self.getReport(mylist[3:6]), name='t2')
        t3 = threading.Thread(target=self.getReport(mylist[6:9]), name='t3')    
        t4 = threading.Thread(target=self.getReport(mylist[9:]), name='t4')  
    
        # starting threads
        t1.start()
        t2.start()
        t3.start()
        t4.start()
    
        # wait until all threads finish
        t1.join()
        t2.join()
        t3.join()
        t4.join()
                        
                        
        return jsonData        
               
               