from cv import IAnalyzer

class SSDeep(IAnalyzer):
    
    def __init__(self, filename, metaID):
        super().__init__()
        self.filename=filename
        self.metaID=metaID
    
    def analyze(self):
        cmd="ssdeep '" + self.filename +"' "
        print(cmd)
        output=self.run(cmd)
        print(output)
        lista=list(output.split("\n"))
        
        i=lista[1] 
        final=list(i.split(","))
    
        return final[0]
             