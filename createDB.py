import pymysql

db=pymysql.connect(user="andreea", password="123", host="localhost", database="licenta")

cursor=db.cursor()
  
def fetchResults(query):
    try:
        cursor.execute(query)
        records = cursor.fetchall()
        return records

    except pymysql.Error as error:
        print("error {}".format(error))
        db.rollback()

def change(query):
    try:
        cursor.execute(query)
        db.commit()

    except pymysql.Error as error:
        print("error {}".format(error))
        db.rollback()

def createMetadataObj(hash):
    change("INSERT INTO Metadata(Hash) VALUES ('"+hash+"') ;")   
    
def getMetadataID(hash):
    record=fetchResults("SELECT ID FROM Metadata WHERE Hash='"+hash+"' ;")
    return record[0][0]    
    
def createAnalysisObj(metadataID, result, tool):
    change("INSERT INTO Analysis(MetadataID, Result, Tool) VALUES("+str(metadataID) +", '"+result+"' , '"+tool+"') ;"); 
    
def createPerformance(detectorType, score, script):
    change("INSERT INTO Performance(detectorType, score, script) VALUES ('"+detectorType+"', "+ score+" , '"+script+"') ;");        
              
        