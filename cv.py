import subprocess


class IAnalyzer:
  
  def run(self,cmd):
    completed = subprocess.run(["powershell", "-Command", cmd], capture_output=True)
    return completed.stdout.decode('utf-8')

# cmd=IAnalyzer()
# string="Get-FileHash -Path C:\\Users\\Ade\\Desktop\\Licenta\\Proiect\\uploads\\a051e7d40787f2161f07aafa3297bb28dc95286ac9abb68c7fb379036bfa5615.dll -Algorithm SHA256"
# output = cmd.run(string)
# lista=list(output.split("\n"))
# i=lista[3] 
# final=list(i.split(" ")) 
# print("Hash File is", final[10])

