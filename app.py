import json
import os
from flask import Flask, jsonify, request, json
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import null

from unzip import Unzipper
from total_analyze import TotalAnalyzer

app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///tmp/MyDB.db'

# db = SQLAlchemy(app)
app.config["FILE_UPLOADS"] = "C:\\Users\\Ade\\Desktop\\Licenta\\Proiect\\uploads"

# m=Metadata(Hash="81")
# p=Analysis(Result="mara se duce la piata", Tool="Yara")
# m.rel=p
# db.session.add(m)
# db.session.commit()

# def Analysis_serializer(ex):
#     return {
#         'ID': ex.ID,
#         'MetadataID': ex.MetadataID,
#         'Result':ex.Result,
#         'Tool': ex.Tool,
#         'Result': ex.Result        
#     }
    
# def Metadata_serializer(ex):
#     return {
#         'ID': ex.ID,
#         'Hash': ex.Hash,
#         'Timestamp': ex.Timestamp
#     }    

# @app.route('/app', methods=['GET'])
# def index():
#     return jsonify([*map(Analysis_serializer, Analysis.query.all())])

@app.route('/upload', methods=['POST'])    
def create():
    
    if request.method == "POST":

        if request.files:

            file = request.files["file"]

            file.save(os.path.join(app.config["FILE_UPLOADS"], file.filename))
            print("File saved")
            myzip=Unzipper(file.filename)
            new_path = myzip.unzip()
            tool=TotalAnalyzer(new_path)
            tool.analyze()
            data={}
            data['message'] = tool.returnReport()
            tool.delete()
            return jsonify(data), 200

    
# @app.route('/app/<id>')    
# def show(id):
#     return jsonify([*map(example_serializer, Example.query.filter_by(id=id))])

    
if __name__ == '__main__':
    app.run(debug=True)